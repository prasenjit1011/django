from django.http import HttpResponse
from django.shortcuts import render
from .models import Product

def index(request):
    pagename = 'Shop Page'
    fname = request.POST.get('fname','Robin Hood')
    data = {'pagename':pagename,'fname':fname, 'coin_name':'Bitcoin', 'price':7845}
    return render(request,'index.html',data)

def product(request):
    pagename = 'product Page'
    fname = request.POST.get('fname','Robin Hood')
    products = Product.objects.all()
    for x in products:
        print('----------')
        print(x)

    data = {'pagename':pagename,'products':products,'fname':fname, 'coin_name':'Bitcoin', 'price':7845}
    return render(request,'product.html',data)
