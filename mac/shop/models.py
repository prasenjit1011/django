from django.db import models

# Create your models here.
class Product(models.Model):
    product_id=models.AutoField
    product_name=models.CharField(max_length=255)
    product_desc=models.CharField(max_length=255)
    product_price=models.IntegerField()
    product_date=models.DateField()
    product_image=models.ImageField(upload_to="shop/images",default="")
    category_id=models.CharField(max_length=255,default="")

    def __str__(self):
        return  self.product_name
